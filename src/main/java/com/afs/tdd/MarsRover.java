package com.afs.tdd;

import java.util.Arrays;

public class MarsRover {
    private Position position;
    private String command;

    public MarsRover(Position position, String command) {
        this.position = position;
        this.command = command;
    }

    public void executeCommand(Position position, String command) {
        if (command.equals("M")) {
            moveForward(position.getDirection());
        }
        if (command.equals("L")) {
            turnLeft(position.getDirection());
        }
        if (command.equals("R")) {
            turnRight(position.getDirection());
        }
        if (command.length() > 1) {
            multipleCommands(command);
        }
    }

    private void multipleCommands(String command) {
        Arrays.stream(command.split(""))
                .forEach(oneCommand ->
                        executeCommand(getPosition(), oneCommand));
    }

    private void moveForward(String start_direction) {
        if (start_direction.equals("N")) {
            position.setY(position.getY() + 1);
        }
        if (start_direction.equals("S")) {
            position.setY(position.getY() - 1);
        }
        if (start_direction.equals("E")) {
            position.setX(position.getX() + 1);
        }
        if (start_direction.equals("W")) {
            position.setX(position.getX() - 1);
        }
    }

    private void turnLeft(String start_direction) {
        if (start_direction.equals("N")) {
            position.setDirection("W");
        }
        if (start_direction.equals("S")) {
            position.setDirection("E");
        }
        if (start_direction.equals("E")) {
            position.setDirection("N");
        }
        if (start_direction.equals("W")) {
            position.setDirection("S");
        }
    }

    private void turnRight(String start_direction) {
        if (start_direction.equals("N")) {
            position.setDirection("E");
        }
        if (start_direction.equals("S")) {
            position.setDirection("W");
        }
        if (start_direction.equals("E")) {
            position.setDirection("S");
        }
        if (start_direction.equals("W")) {
            position.setDirection("N");
        }
    }

    public Position getPosition() {
        return position;
    }
}
